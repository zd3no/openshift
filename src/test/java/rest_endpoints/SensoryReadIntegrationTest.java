package rest_endpoints;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;
import javax.ws.rs.core.Response.Status;

import model.SensorReading;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.jboss.shrinkwrap.resolver.api.maven.PomEquippedResolveStage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(Arquillian.class)
public class SensoryReadIntegrationTest {

    @Inject
    private SensoryRead sensoryRead;

    @PersistenceContext
    private EntityManager em;

    Random rand = new Random(System.currentTimeMillis());

    @Inject
    UserTransaction utx;

    @Deployment
    public static Archive<?> createTestArchive() {
        PomEquippedResolveStage pom = Maven.resolver().loadPomFromFile("pom.xml").importRuntimeAndTestDependencies();
        File[] libs = pom.resolve("org.slf4j:slf4j-api").withTransitivity().asFile();
        WebArchive war = ShrinkWrap.create(WebArchive.class, "test.war")
                .addPackages(true, "api", "config", "model", "dps", "rest_endpoints")
                .addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
                .addAsWebResource(EmptyAsset.INSTANCE, "beans.xml");
        return war.addAsLibraries(libs);
    }

    @Before
    public void setUp() throws Exception {
        utx.begin();
        em.joinTransaction();
        em.createQuery("DELETE FROM SensorReading s").executeUpdate();
        utx.commit();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void persistSensorReadings_testInvalidInput_shouldReturnErrorCode() {
        assertEquals(sensoryRead.persistSensorReadings(null).getStatus(), Status.NO_CONTENT.getStatusCode());
        assertEquals(sensoryRead.persistSensorReadings(new HashSet<SensorReading>()).getStatus(), Status.NO_CONTENT.getStatusCode());
        assertEquals(sensoryRead.persistSensorReading(null, null).getStatus(), Status.NO_CONTENT.getStatusCode());
        assertEquals(sensoryRead.persistSensorReading("myId", null).getStatus(), Status.NO_CONTENT.getStatusCode());
    }

    @Test
    public void persistSensorReading_testValidInput_persistToDB() {
        assertEquals(sensoryRead.persistSensorReading("myId", generateSensorReading()).getStatus(), Status.CREATED.getStatusCode());
        assertEquals(getCollectionsFromDB().size(), 1);
    }

    @Test
    public void persistSensorReadings_testValidInput_shouldPersistToDB() {
        ArrayList<SensorReading> list = new ArrayList<>();
        list.add(generateSensorReading());
        list.add(generateSensorReading());
        list.add(generateSensorReading());
        assertEquals(sensoryRead.persistSensorReadings(list).getStatus(), Status.CREATED.getStatusCode());
        assertEquals(getCollectionsFromDB().size(), 3);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void getAll_testRetreivingExistingData_shoudReturnCollection() {
        ArrayList<SensorReading> list = new ArrayList<>();
        list.add(generateSensorReading());
        list.add(generateSensorReading());
        list.add(generateSensorReading());
        sensoryRead.persistSensorReadings(list);
        assertEquals(((Collection<SensorReading>) sensoryRead.getAll().getEntity()).size(), 3);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void getAll_testRetreivingDataFromEmptyTable_shoudReturnEmptyCollection() {
        assertEquals(((Collection<SensorReading>) sensoryRead.getAll().getEntity()).size(), 0);
    }

    @SuppressWarnings({"unchecked", "deprecation"})
    @Test
    public void getAllSorted_testRetreivingExistingData_shoudReturnCollection() {
        ArrayList<SensorReading> list = new ArrayList<>();
        SensorReading sensorReading = generateSensorReading();
        sensorReading.setDate(new Date(2015, 1, 3));
        list.add(sensorReading);
        sensorReading = generateSensorReading();
        sensorReading.setDate(new Date(2015, 1, 2));
        list.add(sensorReading);
        sensorReading = generateSensorReading();
        sensorReading.setDate(new Date(2015, 1, 5));
        list.add(sensorReading);
        sensoryRead.persistSensorReadings(list);
        List<SensorReading> listt = (List<SensorReading>) ((Collection<SensorReading>) sensoryRead.getAll().getEntity());
        assertTrue(listt.get(0).getDate().after(listt.get(1).getDate()));
        assertFalse(listt.get(1).getDate().before(listt.get(2).getDate()));
    }

    private SensorReading generateSensorReading() {
        SensorReading sensorReading = new SensorReading();
        sensorReading.setActive(rand.nextBoolean());
        sensorReading.setDate(new Date());
        sensorReading.setHumidity(rand.nextDouble() * 10);
        sensorReading.setSensorHeight(rand.nextDouble() * 10);
        sensorReading.setSensorId("sensorId");
        sensorReading.setTemperature(rand.nextDouble() * 10);
        return sensorReading;
    }

    public Collection<SensorReading> getCollectionsFromDB() {
        Collection<SensorReading> list = null;
        try {
            utx.begin();
            em.joinTransaction();
            list = em.createQuery("FROM SensorReading i", SensorReading.class).getResultList();
            utx.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

}
