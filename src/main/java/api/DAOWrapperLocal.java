package api;

import java.util.Collection;

import javax.ejb.Local;

@Local
public interface DAOWrapperLocal {

	/**
	 * Persists a valid Entity to database. Please refer to the available
	 * models.
	 *
	 * @param entity
	 *            - The entity to persist
	 * @return - true if persist was successful, false otherwise
	 */
	boolean persist(Object entity);

	/**
	 * Persists a group of entities to database as a single logical transaction.
	 * Please refer to the available models for Entity types.
	 *
	 * @param entities
	 *            - Collection of entities to persist
	 * @return - true if persist was successful, false otherwise.
	 */
	boolean persist(Collection<?> entities);

	/**
	 * Return all entities from DB that match the provided class.
	 *
	 * @param clazz
	 *            - The entity class to extract from DB
	 * @return - Collection of entities, or empty collection
	 * @throws ClassNotFoundException
	 *             - If the specified class is not a valid Entity
	 */
	Collection<?> getAll(Class<?> clazz) throws ClassNotFoundException;

	/**
	 * Return all entities from DB that match the provided class sorted by the
	 * provided sort string in the provided sort order.
	 * 
	 * @param clazz
	 *            - The entity class to extract from DB
	 * @param sortBy
	 *            - Sort string attribute of the provided class
	 * @param sort
	 *            - Sort type enum (ASC or DESC)
	 * @return - Collection of entities, or empty collection
	 * @throws ClassNotFoundException
	 *             - If the specified class is not a valid Entity
	 */
	Collection<?> getAllSorted(Class<?> clazz, String sortBy, Sort sort) throws ClassNotFoundException;

}
