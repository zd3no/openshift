package api;

import javax.ejb.Singleton;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * This class allows us to use dependency injection to create a logger instance
 * in every location of our application like this: <code>@Inject
 * Logger log;</code>
 * </p>
 * <p>
 * Using a producer allows us to obtain information about the injection point
 * and use this information to extract the package/class name and create a new
 * logger instance with it
 * </p>
 *
 */
@Singleton
public class LogProvider {

	@Produces
	public Logger createLogger(InjectionPoint ip) {
		return LoggerFactory.getLogger(ip.getMember().getDeclaringClass());
	}
}