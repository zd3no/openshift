package dps;

import java.util.Collection;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;

import api.DAOWrapperLocal;
import api.Sort;

@Stateless
public class DAOWrapperLocalImpl implements DAOWrapperLocal {

	@PersistenceContext
	private EntityManager	em;

	@Inject
	private Logger			log;

	@Override
	public boolean persist(Object entity) {
		try {
			log.debug("About to persist {} to DB",
					entity.getClass().getName());
			em.persist(entity);
		} catch (IllegalArgumentException e) {
			log.error("Tried to persist a non-entity ({}) and failed", entity
					.getClass().getName());
			return false;
		} catch (EntityExistsException e) {
			log.error("Cannot persist! Duplicate entity {}",
					entity.toString());
			return false;
		}
		log.debug("Entity was persisted successfully");
		return true;
	}

	@Override
	public boolean persist(Collection<?> entities) {
		log.debug("About to persist a collection of {} {} to DB",
				entities.size(), entities.getClass().getName());
		for (Object entity : entities) {
			try {
				em.persist(entity);
			} catch (IllegalArgumentException e) {
				log.error("Tried to persist a non-entity ({}) and failed",
						entity.getClass().getName());
				return false;
			} catch (EntityExistsException e) {
				log.error("Cannot persist! Duplicate entity {}",
						entity.toString());
				return false;
			}
		}
		log.debug("{} entities were persisted successfully",
				entities.size());
		return true;
	}

	@Override
	public Collection<?> getAll(Class<?> clazz) throws ClassNotFoundException {
		return em.createQuery("SELECT x FROM " + clazz.getCanonicalName() + " as x", clazz).getResultList();
	}

	@Override
	public Collection<?> getAllSorted(Class<?> clazz, String sortBy, Sort sort) throws ClassNotFoundException {
		return em.createQuery("SELECT x FROM " + clazz.getCanonicalName() + " as x ORDER BY x." + sortBy + " " + sort.name(), clazz).getResultList();
	}

}
