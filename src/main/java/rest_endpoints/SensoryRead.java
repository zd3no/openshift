package rest_endpoints;

import java.util.Collection;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import model.SensorReading;

import org.slf4j.Logger;

import api.DAOWrapperLocal;
import api.Sort;

@Path("/reading")
public class SensoryRead {

	@EJB
	private DAOWrapperLocal	dao;

	@Inject
	private Logger			log;

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public Response persistSensorReadings(
			Collection<SensorReading> sensorReadings) {
		if (sensorReadings == null || sensorReadings.isEmpty()) {
			log.error("Tried to persist null or empty Collection<SensorReading>");
			return Response.ok().status(Status.NO_CONTENT).build();
		}
		boolean successfulPersist = dao.persist(sensorReadings);
		Status status;
		if (successfulPersist) {
			status = Status.CREATED;
		} else {
			status = Status.NOT_MODIFIED;
		}
		return Response.ok().status(status).build();
	}

	@PUT
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public Response persistSensorReading(@PathParam("id") String id,
			SensorReading sensorReading) {
		if (sensorReading == null) {
			log.error("Tried to persist null SensorReading with id {}", id);
			return Response.ok().status(Status.NO_CONTENT).build();
		}

		boolean successfulPersist = dao.persist(sensorReading);
		Status status;
		if (successfulPersist) {
			status = Status.CREATED;
		} else {
			status = Status.NOT_MODIFIED;
		}
		return Response.ok().status(status).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAll() {
		try {
			Status status;
			@SuppressWarnings("unchecked")
			Collection<SensorReading> list = (Collection<SensorReading>) dao.getAllSorted(SensorReading.class, "date", Sort.DESC);
			if (list.size() == 0) {
				status = Status.NO_CONTENT;
			} else {
				status = Status.OK;
			}
			return Response.ok().entity(list).status(status).build();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return Response.ok().status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
}
