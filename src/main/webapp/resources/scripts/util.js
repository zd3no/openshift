define("util", [ 'jquery' ], function($) {

	var object = {
		loadCSS : function(page) {
			console.log("Adding css to page: ");
			var header = $('head');
			var styleSheet = document.createElement('link');
			styleSheet.rel = 'stylesheet';
			styleSheet.type = 'text/css';
			styleSheet.href = 'resources/styles/main.css';
			styleSheet.media = 'all';
			header.append(styleSheet);
			var styleSheet = document.createElement('link');
			styleSheet.rel = 'stylesheet';
			styleSheet.type = 'text/css';
			styleSheet.href = 'resources/styles/slick.css';
			styleSheet.media = 'all';
			header.append(styleSheet);
			var styleSheet = document.createElement('link');
			styleSheet.rel = 'stylesheet';
			styleSheet.type = 'text/css';
			styleSheet.href = 'resources/styles/slick-theme.css';
			styleSheet.media = 'all';
			header.append(styleSheet);
		},

		addDetailsFunctionality : function() {
			if (this.get_browser_info().name == 'Firefox') {
				$("details>div").css({
					"display" : "none"
				});
				var summaryList = document.getElementsByTagName('summary');
				for (var i = 0; i < summaryList.length; i++) {
					summaryList[i].innerHTML = "<b>&#x27b4;</b> " + summaryList[i].innerHTML;
				}
				$('body').on('click', 'details summary', function() {
					$(this).parent().find('div').toggle();
					var summaryText = $(this).text();
					if ($(this).parent().find('div').css("display") == "none") {
						$(this).html(summaryText.replace(summaryText.split(' ', 1), "<b style='color:green'>&#x27b4;</b> "));
					} else {
						$(this).html(summaryText.replace(summaryText.split(' ', 1), "<b style='color:orange'>&#x27b6;</b> "));
					}
				});
			}
		},

		get_browser_info : function() {
			var ua = navigator.userAgent, tem, M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
			if (/trident/i.test(M[1])) {
				tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
				return {
					name : 'IE',
					version : (tem[1] || '')
				};
			}
			if (M[1] === 'Chrome') {
				tem = ua.match(/\bOPR\/(\d+)/)
				if (tem != null) {
					return {
						name : 'Opera',
						version : tem[1]
					};
				}
			}
			M = M[2] ? [ M[1], M[2] ] : [ navigator.appName, navigator.appVersion, '-?' ];
			if ((tem = ua.match(/version\/(\d+)/i)) != null) {
				M.splice(1, 1, tem[1]);
			}
			return {
				name : M[0],
				version : M[1]
			};
		}.bind(this),

		tableLanguage : {
			"lengthMenu" : "Arat&#259; _MENU_ &#238;nregistr&#259;ri pe pagin&#259;",
			"zeroRecords" : "Nimic nu a fost g&#259;sit in baza de date.",
			"info" : "Pagina _PAGE_ din _PAGES_",
			"infoEmpty" : "Nu sunt &#238;nregistr&#259;ri in baza de date.",
			"infoFiltered" : "(Filtrat din totalul de _MAX_ &#238;nregistr&#259;ri)",
			"search" : "Caut&#259;"
		},

		tableColumns : [ {
			title : "id",
			mData : "id",
			visible : false
		}, {
			title : "sensor id",
			mData : "sensorId",
		}, {
			title : "dată",
			mData : "date",
			render : function(data, type, row, meta) {
				date = new Date(data);
				hour = date.getHours();
				hoursString = hour < 10 ? "0"+hour:hour;
				return date.toLocaleDateString() + " " + hoursString+":00";
			}
		}, {
			title : "înălțime",
			mData : "sensorHeight",
			render : function(data, type, row, meta) {
				return data + " m";
			}
		}, {
			title : "activ",
			mData : "active",
			render : function(data, type, row, meta) {
				return data ? "Da" : "Nu";
			}
		}, {
			title : "temperatură",
			mData : "temperature",
			render : function(data, type, row, meta) {
				return data + " &deg;C";
			}
		}, {
			title : "umiditate",
			mData : "humidity",
			render : function(data, type, row, meta) {
				return data + " %";
			}
		} ]
	};
	return object;
});