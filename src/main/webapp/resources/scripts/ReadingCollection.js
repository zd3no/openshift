define(function(){

    return function ReadingCollection() {
        this.keys = [];
        this.last24hData = [];
        this.yesterdayDate = new Date() - 24 * 60 * 60 * 1000;

        this.add = function (reading){
            var readingDate = new Date(reading.date);
            var readingDateMonthDay = readingDate.getDate() < 10 ? "0"+readingDate.getDate() : readingDate.getDate();
            var readingDateMonth = readingDate.getMonth() < 10 ? "0"+readingDate.getMonth() : readingDate.getMonth();
            this.readingKey = "_"+readingDateMonthDay+"_"+readingDateMonth;
            if(this.keys.indexOf(this.readingKey) >=0){
                this[this.readingKey].count += 1;
                this[this.readingKey].temperature += reading.temperature;
                this[this.readingKey].humidity += reading.humidity;
            }else{
                this.keys.push(this.readingKey);
                this[this.readingKey] = new Object();
                this[this.readingKey].count = 1;
                this[this.readingKey].temperature = reading.temperature;
                this[this.readingKey].humidity = reading.humidity;
            }
            if(reading.date > this.yesterdayDate){
                this.last24hData.push(reading);
            }
        };

        this.getAveragedReadingsCollection = function(){
            var collection = [];
            for(var i=0; i< this.keys.length; i++){
                var temp = this[this.keys[i]].temperature / this[this.keys[i]].count;
                var hum = this[this.keys[i]].humidity / this[this.keys[i]].count;
                var object = {
                    temperature: temp,
                    humidity: hum
                }
                collection.push(object);
            }
            return collection;
        };

        this.getLast24hData = function(){
            return last24hData;
        }
    }
})