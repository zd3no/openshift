requirejs.config({
	baseUrl : 'resources/scripts/lib',
	paths : {
		jquery : 'jquery',
		util : '../util',
		ReadingCollection: '../ReadingCollection',
		modernizr : 'modernizr',
		datatables : 'jquery.dataTables.min',
		chart : 'Chart',
		slick : 'slick'
	}
});

require([ 'jquery', 'util', 'tableUtil' ], function($, util, tableUtil) {
	util.loadCSS(window.location.href);
	util.addDetailsFunctionality();
	tableUtil.loadTable();
});

define("tableUtil", [ 'jquery', 'datatables', 'util', 'chartjs' ], function($, datatables, util, chartjs) {

	var me = {
		loadTable : function() {
			$.ajax({
				url : "rest/reading/",
				type : "GET",
				dataType : "json",
				success : function(response) {
					// calls createChart after 0ms, but lets the flow continue.
					// ASYNC functionality
					setTimeout(chartjs.createChart(response), 0);
					setTimeout(me.createTable(response), 0);
				},
				error : function(message) {
					alert(message);
				}
			});
		},

		createTable : function(response) {
			console.log("Building table with %s elements", response.length);
			$('#dataTable').html('<table cellspacing="0" id="queryResults"></table>');
			myTable = $('#queryResults').dataTable({
				language : util.tableLanguage,
				data : response,
				bAutoWidth : false,
				bPaginate : true,
				deferRender : true,
				search : true,
				sort : true,
				order : [ [ 2, "desc" ] ],
				columns : util.tableColumns,
				aoColumnDefs : [ {
					"sDefaultContent" : '-',
					"aTargets" : [ '_all' ]
				} ]
			});
			console.log("Table created successfully");
		}
	}

	return me;
});

define("chartjs", [ 'jquery', 'chart', 'carousel', 'ReadingCollection' ], function($, Chart, carousel,
ReadingCollection) {
	var me = {
		createChart : function(responseData) {
			var chartData = responseData;
			chartData.reverse();
			console.log("Creating charts...");
			var lastMonthData = me.extractLastMonthData(chartData);
			var last24hData = me.extract24hData(lastMonthData);
			if (lastMonthData.length == 0) {
				console.log("No data for charts");
				return;
			}
			me.extractSensors(lastMonthData);
			me.extractDayLabels(last24hData);
			me.extractMonthLabels(lastMonthData);

			if(last24hData.length != 0){
				//create chart for day view
				var dayDatasets = me.makeChartDataset(last24hData, true);
				for (var i = 0; i < me.sensors.length; i++) {
					var data = {
						labels : me.dayLabels,
						datasets : dayDatasets[i]
					};
					$('<div><p class="chartTitle">' + me.sensors[i] + '</p><canvas id="chart' + i + '" width="1000" height="500"></canvas></div>')
							.appendTo('#charts');
					var ctx = $("#chart" + i).get(0).getContext("2d");
					var myNewChart = new Chart(ctx).Line(data, {
						multiTooltipTemplate : "<%= value %> <%= datasetLabel %>",
						scaleFontSize : 12,
						scaleFontStyle : "normal",
						scaleFontColor : "#fff",
						animation: false
					});
				}
			}

			//create chart for month view
			var monthDatasets = me.makeChartDataset(lastMonthData, false);
			for (var i = 0; i < me.sensors.length; i++) {
				var data = {
					labels : me.monthLabels,
					datasets : monthDatasets[i]
				};
				$('<div><p class="chartTitle">' + me.sensors[i] + '</p><canvas id="monthlyChart' + i + '" width="1000" height="500"></canvas></div>')
						.appendTo('#chartsMonthly');
				var ctx = $("#monthlyChart" + i).get(0).getContext("2d");
				var myNewChart = new Chart(ctx).Line(data, {
					multiTooltipTemplate : "<%= value %> <%= datasetLabel %>",
					scaleFontSize : 12,
					scaleFontStyle : "normal",
					scaleFontColor : "#fff",
					animation: false
				});
			}

			console.log("Charts created successfully");
			carousel.load();
		},

		extractLastMonthData : function (responseData){
		    var lastMonth = new Date() - 30 * 24 * 60 * 60 * 1000;
		    return responseData.filter(function(value) {
                if (value.date >= lastMonth) {
                    return value;
                }
            });
		},

		extract24hData : function(responseData) {
			var yesterday = new Date() - 24 * 60 * 60 * 1000;
			return responseData.filter(function(value) {
				if (value.date >= yesterday) {
					return value;
				}
			});
		},

		extractDayLabels : function(data) {
			var monthNames = [ "Jan", "Feb", "Mar", "Apr", "Mai", "Iun", "Iul", "Aug", "Sep", "Oct", "Nov", "Dec" ];
			var set = new Set();
			for (var i = 0; i < data.length; i++) {
				var mdate = new Date(data[i].date);
				var hour = mdate.getHours();
                var hoursString = hour < 10 ? "0"+hour:hour;
				set.add(mdate.getDate() + " " + monthNames[mdate.getMonth()] + " " + hoursString + ":00");
			}
		},

		extractMonthLabels : function(data) {
            var monthNames = [ "Jan", "Feb", "Mar", "Apr", "Mai", "Iun", "Iul", "Aug", "Sep", "Oct", "Nov", "Dec" ];
            var set = new Set();
            for (var i = 0; i < data.length; i++) {
                var mdate = new Date(data[i].date);
                set.add(mdate.getDate() + " " + monthNames[mdate.getMonth()]);
            }
            set.forEach(function(value) {
                me.monthLabels.push(value);
            });
        },

		extractSensors : function(data) {
			var set = new Set();
			for (var i = 0; i < data.length; i++) {
				set.add(data[i].sensorId);
			}
			set.forEach(function(value) {
				me.sensors.push(value);
			});
		},

		makeChartDataset : function(mdata, isDatasetForLast24hData) {
			var dataset = [];
			for (var i = 0; i < me.sensors.length; i++) {
                var readingCollection = new ReadingCollection();
                mdata.filter(function (object){
                    if(object.sensorId == me.sensors[i])
                        readingCollection.add(object);
                })
                
                var temperatureDataForThisSensor = [];
                var humidityDataForThisSensor = [];
                if(isDatasetForLast24hData){
                    temperatureDataForThisSensor = readingCollection.getLast24hData().map(function (object) {
                        return object.temperature;
                    })
                    humidityDataForThisSensor = readingCollection.getLast24hData().map(function(object) {
                        return object.humidity;
                    })
                }else{
                    temperatureDataForThisSensor = readingCollection.getAveragedReadingsCollection().map(function(object) {
                        return object.temperature;
                    })
                    humidityDataForThisSensor = readingCollection.getAveragedReadingsCollection().map(function(object) {
                        return object.humidity;
                    })
                }
				dataset.push(
				[{
					label : "℃",
					fillColor : "rgba(220,220,220,0.0)",
					strokeColor : "red",
					pointColor : "red",
					pointStrokeColor : "#fff",
					pointHighlightFill : "#fff",
					pointHighlightStroke : "rgba(220,220,220,1)",
					data : temperatureDataForThisSensor
				},{
					label : "%",
					fillColor : "rgba(220,220,220,0.0)",
					strokeColor : "yellow",
					pointColor : "yellow",
					pointStrokeColor : "#fff",
					pointHighlightFill : "#fff",
					pointHighlightStroke : "rgba(220,220,220,1)",
					data : humidityDataForThisSensor
				}]);
			}
			return dataset;
		},

		sensors : [],
		monthLabels : [],
		dayLabels : []
	}
	return me;
});

define("carousel", [ 'slick', 'jquery' ], function(Slick, $) {
	var me = {
		load : function() {
			$('#charts').slick({
				dots : true,
				infinite : true,
				speed : 400,
				fade : true,
				cssEase : 'linear'
			});

			$('#chartsMonthly').slick({
				dots : true,
				infinite : true,
				speed : 400,
				fade : true,
				cssEase : 'linear'
			 });
		}
	}
	return me;
});
